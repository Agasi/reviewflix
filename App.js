import React from 'react';
import {Button, StyleSheet, Text, View, TextInput, ScrollView } from 'react-native';
import {createBottomTabNavigator, createAppContainer, TabNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons'


export class Login extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    }
  }
  onPressEnviarDatos= () => {
    alert(this.state.typedEmail+this.state.typedPassword)

  }
  render() {
    return (
      <View style={styles.container}>
            <Text style={styles.titulo}> ⌨️ Logueate para continuar 🔓️ </Text>
        <Text style={{fontWeight:'bold',lineHeight: 30}}>Ingrese su correo</Text>
        <TextInput style={{
          height:40,
          borderColor: 'gray',
          borderWidth: 1
        }}
        keyboardType='email-address'
        placeholder=' Ingrese su e-mail'
        onChangeText={
          (text)=> {
            this.setState(
              (previousState)=> {
                return {
                  typedEmail: text
                };
              }
            )
          }
        }
        ></TextInput>
        <Text></Text>
        <Text style={{fontWeight:'bold',lineHeight: 30}}>Ingrese su contraseña</Text>
        <TextInput style={ {
          height:40,
          borderColor: 'gray',
          borderWidth: 1
        }}
        placeholder=' Ingrese su contraseña'
        secureTextEntry={true}
        onChangeText={(text) => {this.setState(() => {
          return {
            typedPassword: text
          };
        })}}
        ></TextInput>
        <Text></Text>
        <View style={styles.buttonContainer}>
        <Button onPress={this.onPressEnviarDatos}
                title="Logearse"
                color="white"
                accessibilityLabel="Saber mas"
                ></Button>
                </View>
      </View>
    );
  }
}

export class Recomendado extends React.Component {

  render() {
    return (
      <ScrollView style={styles.scroll}>
        <Text style={styles.titulo}> 🎞️ Peliculas recomendadas ⭐ para vos 🎥</Text>
      </ScrollView>
    );
  }
}

export class Buscar extends React.Component {

  render() {
    return (
      <ScrollView style={styles.scroll}>
      <Text style={styles.titulo}> 🔍 Buscá una película para evaluar 🕵️ </Text>
        <TextInput></TextInput>
      </ScrollView>
    );
  }
}

const RootStack = createBottomTabNavigator({
  Login: Login,
  Recomendado: {
    screen: Recomendado,
    navigationOptions:{
      tabBArLabel: 'Recomendado',
      tabBarIcon:({tintColor})=> (
        <Icon name="ios-star" color={tintColor} size={30}/>
      )
    }
  },
  Buscar: {
    screen: Buscar,
    navigationOptions:{
      tabBArLabel: 'Buscar',
      tabBarIcon:({tintColor})=> (
        <Icon name="ios-search" color={tintColor} size={30}/>
      )
    }
  }

},{
  navigationOptions: {
    tabBarVisible: false
  },
  tabBarOptions:{
    activeTintColor:'teal',
    inactiveTintColor:'grey'
  }
});

const App = createAppContainer(RootStack);


export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:20,
    backgroundColor: '#add8e6',
    justifyContent: 'center',
    
  },
  titulo: {
    flex: 1,
    padding:20,
    backgroundColor: '#add8e6',
    color:'teal',
    fontSize: 35,
    fontWeight: 'bold',
    fontFamily: 'Verdana',
  },
    
  scroll: {
    flex: 1,
    padding:20,
    backgroundColor: '#add8e6',
  },
  buttonContainer: {
    backgroundColor: '#2E9298',
    fontWeight:"bold",
    borderRadius: 10,
    padding: 10,
    justifyContent:'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 10,
    shadowOpacity: 0.25
  }
  
});

